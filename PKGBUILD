# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=hunspell-sk
pkgver=2.4.8
pkgrel=1
pkgdesc="Slovak dictionary for Hunspell"
arch=('any')
url="https://github.com/sk-spell/hunspell-sk"
license=('GPL-2.0-only AND LGPL-2.1-only AND MPL-1.1')
makedepends=('qt6-webengine')
optdepends=('hunspell: the spell checking libraries and apps')
provides=('hunspell-dictionary')
source=("https://github.com/sk-spell/hunspell-sk/releases/download/v$pkgver/$pkgver-sk@dictionaries.addons.mozilla.org.xpi")
sha256sums=('68c360b35ca02c2a0d99389d7558de380206ee1a6b571cc8a68c402c4f15f73e')

package() {
  cd dictionaries
  install -Dm644 sk_SK.* -t "$pkgdir/usr/share/hunspell/"

  # myspell symlinks
  install -d "$pkgdir/usr/share/myspell/dicts"
  pushd "$pkgdir/usr/share/myspell/dicts"
    for file in "$pkgdir/usr/share/hunspell/"*; do
      ln -sv /usr/share/hunspell/$(basename "${file}") .
    done
  popd

  # Install webengine dictionaries
  install -d "$pkgdir"/usr/share/qt{,6}/qtwebengine_dictionaries/
  for _file in "$pkgdir"/usr/share/hunspell/*.dic; do
  _filename=$(basename "${_file}")
    /usr/lib/qt6/qwebengine_convert_dict "${_file}" "$pkgdir/usr/share/qt6/qtwebengine_dictionaries/${_filename/\.dic/\.bdic}"
  ln -rs "$pkgdir/usr/share/qt6/qtwebengine_dictionaries/${_filename/\.dic/\.bdic}" "$pkgdir/usr/share/qt/qtwebengine_dictionaries/"
  done

  # docs
  install -Dm644 doc/{AUTHORS,Flagy*} -t "$pkgdir/usr/share/doc/$pkgname/"
}
